package net.persgroep.fortress.domain.enumeration;

/**
 * The EnvironmentType enumeration.
 */
public enum EnvironmentType {
    DEV,TEST,ACC,PROD
}
