'use strict';

angular.module('projectInfoServiceApp')
    .controller('ProjectDetailController', function ($scope, $rootScope, $stateParams, entity, Project, Environment) {
        $scope.project = entity;
        $scope.load = function (id) {
            Project.get({id: id}, function(result) {
                $scope.project = result;
            });
        };
        var unsubscribe = $rootScope.$on('projectInfoServiceApp:projectUpdate', function(event, result) {
            $scope.project = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
