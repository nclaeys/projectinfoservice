'use strict';

angular.module('projectInfoServiceApp')
    .controller('ProjectController', function ($scope, $state, $modal, Project, ProjectSearch) {
      
        $scope.projects = [];
        $scope.loadAll = function() {
            Project.query(function(result) {
               $scope.projects = result;
            });
        };
        $scope.loadAll();


        $scope.search = function () {
            ProjectSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.projects = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.project = {
                name: null,
                description: null,
                documentationLink: null,
                id: null
            };
        };
    });
