/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package net.persgroep.fortress.web.rest.dto;
