package net.persgroep.fortress.domain;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import net.persgroep.fortress.domain.enumeration.EnvironmentType;

/**
 * A Environment.
 */
@Entity
@Table(name = "environment")
@Document(indexName = "environment")
public class Environment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private EnvironmentType type;

    @Column(name = "server")
    private String server;

    @Column(name = "amq_host")
    private String amqHost;

    @Column(name = "amq_port")
    private Integer amqPort;

    @ManyToOne
    private Project project;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EnvironmentType getType() {
        return type;
    }

    public void setType(EnvironmentType type) {
        this.type = type;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getAmqHost() {
        return amqHost;
    }

    public void setAmqHost(String amqHost) {
        this.amqHost = amqHost;
    }

    public Integer getAmqPort() {
        return amqPort;
    }

    public void setAmqPort(Integer amqPort) {
        this.amqPort = amqPort;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Environment environment = (Environment) o;

        if ( ! Objects.equals(id, environment.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Environment{" +
            "id=" + id +
            ", type='" + type + "'" +
            ", server='" + server + "'" +
            ", amqHost='" + amqHost + "'" +
            ", amqPort='" + amqPort + "'" +
            '}';
    }
}
