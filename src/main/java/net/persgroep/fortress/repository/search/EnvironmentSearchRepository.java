package net.persgroep.fortress.repository.search;

import net.persgroep.fortress.domain.Environment;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Environment entity.
 */
public interface EnvironmentSearchRepository extends ElasticsearchRepository<Environment, Long> {
}
