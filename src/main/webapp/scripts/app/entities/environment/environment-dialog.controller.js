'use strict';

angular.module('projectInfoServiceApp').controller('EnvironmentDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Environment', 'Project',
        function($scope, $stateParams, $modalInstance, entity, Environment, Project) {

        $scope.environment = entity;
        $scope.projects = Project.query();
        $scope.load = function(id) {
            Environment.get({id : id}, function(result) {
                $scope.environment = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('projectInfoServiceApp:environmentUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.environment.id != null) {
                Environment.update($scope.environment, onSaveSuccess, onSaveError);
            } else {
                Environment.save($scope.environment, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
