 'use strict';

angular.module('projectInfoServiceApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-projectInfoServiceApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-projectInfoServiceApp-params')});
                }
                return response;
            }
        };
    });
