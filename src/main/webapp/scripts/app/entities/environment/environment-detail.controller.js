'use strict';

angular.module('projectInfoServiceApp')
    .controller('EnvironmentDetailController', function ($scope, $rootScope, $stateParams, entity, Environment, Project) {
        $scope.environment = entity;
        $scope.load = function (id) {
            Environment.get({id: id}, function(result) {
                $scope.environment = result;
            });
        };
        var unsubscribe = $rootScope.$on('projectInfoServiceApp:environmentUpdate', function(event, result) {
            $scope.environment = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
