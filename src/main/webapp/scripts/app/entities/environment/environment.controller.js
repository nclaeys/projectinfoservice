'use strict';

angular.module('projectInfoServiceApp')
    .controller('EnvironmentController', function ($scope, $state, $modal, Environment, EnvironmentSearch) {
      
        $scope.environments = [];
        $scope.loadAll = function() {
            Environment.query(function(result) {
               $scope.environments = result;
            });
        };
        $scope.loadAll();


        $scope.search = function () {
            EnvironmentSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.environments = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.environment = {
                type: null,
                server: null,
                amqHost: null,
                amqPort: null,
                id: null
            };
        };
    });
