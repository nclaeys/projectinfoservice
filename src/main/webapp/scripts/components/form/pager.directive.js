/* globals $ */
'use strict';

angular.module('projectInfoServiceApp')
    .directive('projectInfoServiceAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
