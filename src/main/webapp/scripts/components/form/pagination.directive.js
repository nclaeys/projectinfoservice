/* globals $ */
'use strict';

angular.module('projectInfoServiceApp')
    .directive('projectInfoServiceAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
