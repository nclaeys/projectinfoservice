'use strict';

angular.module('projectInfoServiceApp')
	.controller('EnvironmentDeleteController', function($scope, $modalInstance, entity, Environment) {

        $scope.environment = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Environment.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });