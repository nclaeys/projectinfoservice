'use strict';

angular.module('projectInfoServiceApp')
    .factory('EnvironmentSearch', function ($resource) {
        return $resource('api/_search/environments/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
